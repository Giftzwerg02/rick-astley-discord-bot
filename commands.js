//const rickMeme = require("./commandFunctions/rickMeme.js") 
const fs = require("fs");
const prefix = require("./index.js");
const commandDir = "./commands/";

var commandList = {
    help: ({ details, discord, bot }) => {
        const embed = new discord.MessageEmbed()
            .setColor("#aef")
            .setTitle("Commands!")
            .setAuthor("Rick Astley")
            .setDescription("List of commands of Rick Astley")
            .setThumbnail(bot.user.avatarURL())
            .addField("Command-Prefix", `"${prefix}" (dot)`)
            .addField("\u200B", "\u200B")
            .setTimestamp()
            .setFooter("Never going to give you up!");
        
        for(let [name, desc] of Object.entries(helpList)) {
            
            embed.addField(name, desc);
            
        }

        details.channel.send(embed);
    }
};
var helpList = {};

fs.readdirSync(commandDir).forEach(file => {
    const cmd = require(commandDir + file);
    const name = cmd.name;
    const desc = cmd.desc;
    const func = cmd.func;

    if(!(name && desc && func)) return console.log(`Couldn't load command of file ${commandDir + file}. Continuing without this command...`);

    commandList[name] = func;
    helpList[prefix + name] = desc;
});

module.exports = commandList;

module.exports.notSoFast = function(details) {
    details.channel.send(module.exports.highlightInRed("Not so fast. Intruder detected: Executing code Tango Echo Alpha."));
};

module.exports.highlightInRed = function(text) {
    return "```AsciiDoc\n[" + text + "]\n```";
};