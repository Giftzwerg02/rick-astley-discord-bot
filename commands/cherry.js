module.exports = {
    name: "rickCherry",
    desc: "Executes a Git-*Cherry*-Pick",
    func: ({ details }) => details.channel.send({files: ["img/theRealCherry.png"]})
}