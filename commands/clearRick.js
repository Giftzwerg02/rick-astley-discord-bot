module.exports = {
    name: "clearRick",
    desc: "Clears the rick-channel of a server. OWNER-ONLY!",
    func: ({ details, tools, bot }) => {
        if(!tools.owneronly(details)) return;
        bot.channels.fetch("712387749931384912")
            .then(channel => {
                channel.bulkDelete(99);
            });
    }
}