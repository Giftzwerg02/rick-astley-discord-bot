module.exports = {
    name: "counter",
    desc: "A counter",
    func: ({ details, tools, connectTo, models }) => {
        connectTo("discordbot");
        models.Counter.findOne(
            {
                userID: details.author.id           
            }, 
            (err, counter) => {
                if(err) return console.log(err);
                if(!counter) {
                    tools.save(
                        new models.Counter({
                            userID: details.author.id,
                            username: details.author.username,
                            count: 1
                        })
                    );
                    details.channel.send("This is your first time executing this command!");
                } else {
                    counter.count++;
                    tools.save(counter);
                    details.reply(`This is your ${counter.count}-th time executing this command!`);
                }
            }
        );

    }
}