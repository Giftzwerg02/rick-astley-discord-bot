module.exports = {
    name: "draw",
    desc: "Test",
    func: ({ details, shapes }) => {

        const rect = new shapes.Rectangle(25, 25, 50, 50, {
            fill: "orange",
            stroke: "red"
        });

        const circle = new shapes.Circle(50, 50, 10, {
            fill: "white",
            stroke: "black",
            width: 2
        });

        const designer = shapes.getDesigner();
        designer.addShapes(rect, circle);
        const attachment = designer.drawAttachment(100, 100, "my-rectangle.png", "blue");
        
        details.channel.send("Producing complex image...", attachment);
        
    }
}