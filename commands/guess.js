var ran = Math.floor(Math.random() * 100);

module.exports = {
    name: "guess",
    desc: "Guess the number!",
    func: ({details, args}) => {
        let guess = args[0];
        if(isNaN(guess)) return details.channel.send(`${args[0]} is not a number you dork.`);
        guess = Math.floor(guess);
        if(guess != ran) {

            if(guess < ran) return details.channel.send("Guess higher!");
            if(guess > ran) return details.channel.send("Guess lower!");

        } 

        details.channel.send(`Nice, you got it! The number was ${ran}`);
        ran = Math.floor(Math.random() * 100);

    }
}

