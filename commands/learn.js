module.exports = {
	name: 'learn',
	desc: 'Set and get questions to check your wisdom!',
	func: async ({ discord, details, connectTo, tools, models }) => {
		const set_get_emojis = {
			g: '🇬',
			s: '🇸'
		};

		const next_stop_emojis = {
			n: '▶️',
			s: '⏹'
		};

		const ran_ord_emojis = {
			r: '🇷',
			o: '🇴'
		};

		const cat_que_emojis = {
			c: '🇨',
			q: '🇶'
		};

		const attachment = new discord.MessageAttachment('./img/learning_icon.png', 'learning_icon.png');
		const embed = new discord.MessageEmbed()
			.setColor('#aef')
			.setTitle(`Have fun learning! (kappa)`)
			.attachFiles(attachment)
			.setThumbnail('attachment://learning_icon.png')
			.setDescription(`Press the reactions below and type in commands to navigate through the tree!`)
			.addField('Do you want to learn?', `Then press ${set_get_emojis.g}!`)
			.addField('Do you want to create a new question/category?', `Then press ${set_get_emojis.s}!`)
			.setTimestamp()
			.setFooter('Never going to give you up!');

		const botMsg = await details.channel.send(embed);
		botMsg.react(set_get_emojis.g);
		botMsg.react(set_get_emojis.s);

		const reaction_filter = (reaction, user) => user.id === details.author.id;
		const set_get_choice = await botMsg
			.awaitReactions(reaction_filter, { max: 1, time: 60000 })
			.then((collected) => {
				if (set_get_emojis.s === collected.firstKey()) return 's';
				if (set_get_emojis.g === collected.firstKey()) return 'g';

				details.channel
					.send('Invalid reaction. Please try again later.')
					.then((msg) => msg.delete({ timeout: 3000 }));
				return undefined;
			})
			.catch(console.error);

		botMsg.reactions.removeAll();

		connectTo('discordbot');

		const allSubjects = await models.Subject.find((err, _subject) => {
			return _subject;
		});

		allSubjects.sort((a, b) => (a.index > b.index ? 1 : b.index > a.index ? -1 : 0));

		const filter = (message) => message.author.id === details.author.id;

		if (set_get_choice === 's') {
			const subject = await chooseSubject(details, models, filter, botMsg, embed, allSubjects);

			embed.title = `You chose the subject ${subject.fullN}!`;

			embed.fields = [];
			embed.fields[0] = {
				name: 'Do you want to create a new category?',
				value: `Then choose ${cat_que_emojis.c}!`
			};

			embed.fields[1] = {
				name: 'Do you want to create a new question?',
				value: `Then choose ${cat_que_emojis.q}!`
			};

			botMsg.edit(embed);
			botMsg.react(cat_que_emojis.c);
			botMsg.react(cat_que_emojis.q);

			const cat_que_choice = await botMsg
				.awaitReactions(reaction_filter, { max: 1, time: 60000 })
				.then((collected) => {
					if (cat_que_emojis.c === collected.firstKey()) return 'c';
					if (cat_que_emojis.q === collected.firstKey()) return 'q';

					details.channel
						.send('Invalid reaction. Please try again later.')
						.then((msg) => msg.delete({ timeout: 3000 }));
					return undefined;
				})
				.catch(console.error);

			if (!cat_que_choice) return;

			if (cat_que_choice === 'c') {
				embed.title = 'Please enter the name of your category!';
				embed.fields = [];

				botMsg.edit(embed);

				return await details.channel
					.awaitMessages(filter, { max: 1, time: 60000 })
					.then((collected) => {
						if (!collected.first()) {
							details.channel
								.send('This took too long, please try again.')
								.then((msg) => msg.delete({ timeout: 3000 }));
							return false;
						}
						const catName = collected.first().content;
						let isNew = true;
						isNew = models.Category.findOne(
							{
								name: catName,
								_fk_id: subject._id
							},
							(err, category) => {
								if (!category) return true;

								embed.title = `This category already exists!`;
								embed.description = '';
								embed.fields = [];
								botMsg.edit(embed);
								return false;
							}
						);

						if (!isNew) return;

						models.Category.countDocuments((err, count) => {
							tools.save(
								new models.Category({
									index: count,
									name: catName,
									_fk_id: subject._id
								})
							);
						});

						embed.title = `Created category ${catName}!`;
						embed.description = '';
						embed.fields = [];
						botMsg.edit(embed);
					})
					.catch(console.log);
			} else if (cat_que_choice === 'q') {
				const allCategories = await models.Category.find({ _fk_id: subject._id }, (err, _category) => {
					return _category;
				});

				allCategories.sort((a, b) => (a.index > b.index ? 1 : b.index > a.index ? -1 : 0));

				const category = await chooseCategory(details, models, filter, botMsg, embed, allCategories);

				embed.title = `You chose the category **${category.name}**!`;
				embed.fields = [];
				embed.fields[0] = {
					name: 'Please enter your question!',
					value: '\u200B'
				};

				botMsg.edit(embed);

				const question = await details.channel
					.awaitMessages(filter, { max: 1, time: 60000 })
					.then((collected) => {
						if (!collected.first()) {
							details.channel
								.send('This took too long, please try again.')
								.then((msg) => msg.delete({ timeout: 3000 }));
							return false;
						}
						collected.first().delete();
						return collected.first().content;
					})
					.catch(console.log);

				embed.fields[0] = {
					name: 'Please enter your answer!',
					value: '\u200B'
				};

				botMsg.edit(embed);

				const answer = await details.channel
					.awaitMessages(filter, { max: 1, time: 60000 })
					.then((collected) => {
						if (!collected.first()) {
							details.channel
								.send('This took too long, please try again.')
								.then((msg) => msg.delete({ timeout: 3000 }));
							return false;
						}
						collected.first().delete();
						return collected.first().content;
					})
					.catch(console.log);

				tools.save(
					new models.Question({
						question: question,
						answer: answer,
						_fk_id: category._id
					})
				);

				embed.title = 'Question created!';
				embed.description = 'Preview:';
				embed.fields = [];
				embed.fields[0] = {
					name: `Question: ${question}`,
					value: `Answer: ||${answer}||`
				};

				botMsg.edit(embed);
			}
		} else if (set_get_choice === 'g') {
			const subject = await chooseSubject(details, models, filter, botMsg, embed, allSubjects);
			if (!subject) return;

			embed.title = `You chose the subject ${subject.fullN}!`;
			botMsg.edit(embed);

			const allCategories = await models.Category.find({ _fk_id: subject._id }, (err, _category) => {
				return _category;
			});

			allCategories.sort((a, b) => (a.index > b.index ? 1 : b.index > a.index ? -1 : 0));

			const category = await chooseCategory(details, models, filter, botMsg, embed, allCategories);
			if (!category) return;

			const questions = await models.Question.find({ _fk_id: category._id }, (err, question) => question);

			if (questions.length === 0) {
				embed.title = 'Sorry, but this Category does not have any questions yet!';
				embed.description = 'How about creating some?';
				embed.fields = [];
				botMsg.edit(embed);
				return;
			}

			embed.title = `You chose the category ${category.name}!`;
			embed.fields = [];
			embed.fields[0] = {
				name: 'Do you want to enter **random**-mode?',
				value: `Then press ${ran_ord_emojis.r}`
			};

			embed.fields[1] = {
				name: 'Do you want to enter **ordered**-mode?',
				value: `Then press ${ran_ord_emojis.o}`
			};

			botMsg.edit(embed);

			botMsg.react(ran_ord_emojis.r);
			botMsg.react(ran_ord_emojis.o);

			const ran_ord_choice = await botMsg
				.awaitReactions(reaction_filter, { max: 1, time: 60000 })
				.then((collected) => {
					if (ran_ord_emojis.r === collected.firstKey()) return 'r';
					if (ran_ord_emojis.o === collected.firstKey()) return 'o';

					details.channel
						.send('Invalid reaction. Please try again later.')
						.then((msg) => msg.delete({ timeout: 3000 }));
					return undefined;
				})
				.catch(console.error);

			if (!ran_ord_choice) return;

			botMsg.reactions.removeAll();

			embed.description = `Press ${next_stop_emojis.n} to keep going or ${next_stop_emojis.s} to stop!`;

			embed.fields = [];

			botMsg.edit(embed);

			if (ran_ord_choice === 'r') {
				let counter = 1;
				while (true) {
					const randomQuestion = questions[Math.floor(Math.random() * questions.length)];

					botMsg.react(next_stop_emojis.n);
					botMsg.react(next_stop_emojis.s);

					const keepGoing = await botMsg
						.awaitReactions(reaction_filter, { max: 1, time: 60000 })
						.then((collected) => {
							if (next_stop_emojis.n === collected.firstKey()) return true;
							if (next_stop_emojis.s === collected.firstKey()) return false;

							details.channel
								.send(`Automatically showing next...`)
								.then((msg) => msg.delete({ timeout: 3000 }));
							return true;
						})
						.catch(console.error);

					if (!keepGoing) break;

					embed.title = `Question Nr. ${counter}!`;
					embed.fields = [];
					embed.fields[0] = {
						name: `**Question:** ${randomQuestion.question}`,
						value: `**Answer:** ||${randomQuestion.answer}||`
					};

					botMsg.edit(embed);

					botMsg.reactions.removeAll();

					counter++;
				}

				details.channel.send("That's it!").then((msg) => msg.delete({ timeout: 3000 }));
			} else if (ran_ord_choice === 'o') {
				for (let i = 0; i < questions.length; i++) {
					if (i - 1 === questions.length) break;

					botMsg.react(next_stop_emojis.n);
					botMsg.react(next_stop_emojis.s);

					const keepGoing = await botMsg
						.awaitReactions(reaction_filter, { max: 1, time: 60000 })
						.then((collected) => {
							if (next_stop_emojis.n === collected.firstKey()) return true;
							if (next_stop_emojis.s === collected.firstKey()) return false;

							details.channel
								.send(`Invalid argument: ${content} -> interpreting it as "next"`)
								.then((msg) => msg.delete({ timeout: 3000 }));
							return true;
						})
						.catch(console.error);

					if (!keepGoing) break;

					embed.title = `Question Nr. ${i + 1}!`;
					embed.fields = [];
					embed.fields[0] = {
						name: `**Question:** ${questions[i].question}`,
						value: `**Answer:** ||${questions[i].answer}||`
					};

					botMsg.edit(embed);

					botMsg.reactions.removeAll();
				}

				details.channel.send("That's it!").then((msg) => msg.delete({ timeout: 3000 }));
			}
		}
	}
};

function subjectsToList(subjects) {
	let list = '';
	subjects.forEach((subject) => {
		list += `${subject.index}: ${subject.fullN} (${subject.name})\n`;
	});

	return list;
}

function categoriesToList(categories) {
	let list = '';
	categories.forEach((category) => {
		list += `${category.index}: ${category.name}\n`;
	});

	return list;
}

async function chooseSubject(details, models, filter, botMsg, embed, subjects) {
	embed.fields = [];
	embed.fields[0] = {
		name: 'Choose a subject!',
		value: subjectsToList(subjects) + "\u200B"
	};

	botMsg.edit(embed);

	const choice = await details.channel
		.awaitMessages(filter, { max: 1, time: 60000 })
		.then((collected) => {
			if (!collected.first()) {
				details.channel
					.send('This took too long, please try again.')
					.then((msg) => msg.delete({ timeout: 3000 }));
				return undefined;
			}
			collected.first().delete();
			return collected.first().content;
		})
		.catch(console.log);

	if (!choice) return undefined;

	let subject = undefined;

	if (isNaN(choice)) {
		subject = await models.Subject.findOne(
			{
				name: choice
			},
			(err, _subject) => {
				return _subject;
			}
		);

		if (!subject) {
			subject = await models.Subject.findOne(
				{
					fullN: choice
				},
				(err, _subject) => {
					return _subject;
				}
			);
		}

		if (!subject) {
			details.channel.send('Sorry! This subject does not exists!');
		}
		return subject;
	} else {
		subject = await models.Subject.findOne(
			{
				index: choice
			},
			(err, _subject) => {
				return _subject;
			}
		);

		if (!subject) {
			details.channel.send('Sorry! This subject does not exists!');
		}
	}

	return subject;
}

async function chooseCategory(details, models, filter, botMsg, embed, categories) {
	embed.fields = [];
	embed.fields[0] = {
		name: 'Choose a category!',
		value: categoriesToList(categories) + "\u200B"
	};

	botMsg.edit(embed);

	const choice = await details.channel
		.awaitMessages(filter, { max: 1, time: 60000 })
		.then((collected) => {
			if (!collected.first()) {
				details.channel
					.send('This took too long, please try again.')
					.then((msg) => msg.delete({ timeout: 3000 }));
				return undefined;
			}
			collected.first().delete();
			return collected.first().content;
		})
		.catch(console.log);

	if (!choice) return undefined;

	let category = undefined;

	if (isNaN(choice)) {
		category = await models.Category.findOne(
			{
				name: choice
			},
			(err, _category) => {
				return _category;
			}
		);

		if (!category) {
			category = await models.Category.findOne(
				{
					fullN: choice
				},
				(err, _category) => {
					return _category;
				}
			);
		}

		if (!category) {
			details.channel.send('Sorry! This category does not exists!');
		}

		return category;
	} else {
		category = await models.Category.findOne(
			{
				index: choice
			},
			(err, _category) => {
				return _category;
			}
		);

		if (!category) {
			details.channel.send('Sorry! This category does not exists!');
		}

		return category;
	}
}
