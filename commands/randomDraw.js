module.exports = {
    name: "randomDraw",
    desc: "Test",
    func: ({ details, shapes, args }) => {

        const size = Number(args[0]) || 100;
        const shapeCount = Number(args[1]) || Math.floor(Math.random() * 20) + 1;

        const max = 5000;

        if(size > max || shapeCount > max) return details.channel.send("Never gonna calculate you up, never gonna draw you down.");

        let shapeClassArray = Object.entries(shapes);
        shapeClassArray.shift();

        let shapeArray = [];

        for(let i = 0; i < shapeCount; i++) {

            let randomShape = shapeClassArray[Math.floor(Math.random()*shapeClassArray.length)];
            shapeArray.push(new randomShape[1]().random(size));        

        }

        const designer = shapes.getDesigner();
        designer.addShapes(...shapeArray);
        const attachment = designer.drawAttachment(size, size, "masterpiece.png", "#"+(Math.random()*0xFFFFFF<<0).toString(16));
        
        details.channel.send("Producing complex image...", attachment);
        
    }
}