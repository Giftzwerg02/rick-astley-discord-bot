module.exports = {
    name: "renameRick",
    desc: "Aick Rstley",
    func: ({details, bot, args}) => {
        const name = args.join(" ");
        details.guild.member(bot.user).setNickname(name)
            .catch(err => details.channel.send("The ability to change ones name is important in order to give the common man a sense of freedom."
             + "\nThis freedom has been taken away by the admins of this server!"));
    }
}