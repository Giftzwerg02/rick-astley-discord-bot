module.exports = {
    name: "repeat",
    desc: "rick astley Rick Astley RICK ASTLEY",
    func: ({details, args}) => {
        let repeatText = args.join(" ");
    
        if(/^\s*gonna\s*give\s*you\s*up\s*$/.test(repeatText.trim().toLowerCase())) {
            details.channel.send(`I AM NEVER GOING TO GIVE YOU UP.`);
            return;
        }
    
        details.channel.send(`${repeatText}, ${repeatText}, ${repeatText.toUpperCase()}!`);
    }
}