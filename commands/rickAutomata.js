const CellularAutomata = require("cellular-automata");
module.exports = {
    name: "rickAutomata",
    desc: "Never gonna give Wolfram Automata up.",
    func: ({details, args}) => {
        
        details.channel.send("Little bit of info + all automatas: https://mathworld.wolfram.com/ElementaryCellularAutomaton.html");

        const ruleNumber = Number(args[0]) || 90;
        
        const width = Number(args[1]) || 41;
        if(width > 100) return details.channel.send("Quantum Computers might be out soon but I ain't one.");
        
        const heigth = Number(args[2]) || 20;

        var bits = ruleNumber.toString(2);
        bits = bits.padStart(8, "0");
        bits = bits.split("0").join(" ");
        bits = bits.split("1").join("o");
        
        const ruleSet = {
            "ooo": bits[0],
            "oo ": bits[1],
            "o o": bits[2],
            "o  ": bits[3],
            " oo": bits[4],
            " o ": bits[5],
            "  o": bits[6],
            "   ": bits[7]
        };

        let start = "o";
        const space = " ".repeat(Math.floor(width/2));
        start = space + start + space;
        details.channel.send("```\n" + start + "\n```");
        for(let lines = 0; lines < heigth; lines++) {
            
            let line = "";

            for(let cellIndex = 0; cellIndex < width; cellIndex++) {

                const cell = start.charAt(cellIndex);

                let leftNeighbourState = " "; 
                let rightNeighbourState = " ";

                if(cellIndex != 0) {
                    leftNeighbourState = start.charAt(cellIndex - 1);                 
                }

                if(cellIndex != start.length - 1) {
                    rightNeighbourState = start.charAt(cellIndex + 1);
                }

                line += ruleSet[leftNeighbourState + cell + rightNeighbourState];

            }

            start = line;
            details.channel.send("```\n" + line + "\n```");

        }

        
    }
}