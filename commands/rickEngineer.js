module.exports = {
    name: "rickEngineer",
    desc: "I dare you to crack this puzzle.",
    func: ({ details, args }) => {
        
        const argsText = args.join(" ");
        const nextArg = "61v3m3n3x7";
        const solutionArg = "H4Dn";
    
        const wCordEncrypted = "==ꝹԀʍꝹᖵՈSᒋʞɅԀZʞϺɅZɅꟽʍ∀ɾɅdɥ⅁qSɅʞꓤʇɅ⅁ꟽꓭꓤᖵɅ0IƎɅᴉZᖵq⅁N↊pᒋᖵꓕ⅄XᒋɅꟽɅZᖵǝʇZ⇂ꓕ⅁⇂ɯՈxdɅꟽᴉpHpXʅʅɅߤpʞ⅄Ϻɥ⅁qɅʅXꓕʎƎ⅁NʞᖵɾɅ";
        const nCordEncrypted = {files: ["img/supersafe.png"]};
        const wHint = ":( = Ϻ) ɯɹɐɥɔ-ǝɥʇ-sǝɯᴉʇ-ɥʇ5";
        const pythonFile = {files: ["files/safe.zip"]};
        const bitFile = {files: ["files/safest.txt"]};
        const omgs1 = {files: ["img/omg1.gif", "img/omg2.gif", "img/omg4.gif"]};
        const omgs2 = {files: ["img/omg3.gif", "img/omg5.gif"]};
    
        if(argsText === nextArg) {
            details.channel.send(bitFile);
            return;
        }
    
        if(argsText === solutionArg) {
            details.reply(
                "@everyone OH MY GOD INHUMAN PUZZLE SOLVER. A round of applause for the mad-lad called " + details.author.username.toUpperCase() + " 👏👏👏👏\n" +
                "...unless you cheated, in which case you should go self destruct :-)"
            );
            details.channel.send(omgs1);
            details.channel.send(omgs2);
            return;
        }
    
        details.channel.send(nCordEncrypted);
        details.channel.send(wHint);
        details.channel.send(wCordEncrypted);
        details.channel.send(pythonFile);
        
    
    
        
    }
}

