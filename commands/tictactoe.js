var games = [];


module.exports = {
    name: "tic",
    desc: "Play a game of Tic-Tac-Toe! Start a game with .tic @otherPlayer",
    func: async ({ discord, bot, details, tools, args }) => {
 
        if(!tools.mention.isUserMention(args[0])) return details.channel.send("Please mention another player to start a game!").then(msg => msg.delete({timeout: 5000}));

        const otherPlayerID = tools.mention.getUserID(args[0]);
        
        if(otherPlayerID === details.author.id) return details.channel.send("Is nobody online?").then(msg => msg.delete({timeout: 5000}));
        if(otherPlayerID === bot.user.id) return details.channel.send("Sorry, maybe some other time...").then(msg => msg.delete({timeout: 5000}));

        let isBot = false;
        await tools.getUser.byID(details, otherPlayerID).then(user => isBot = user.bot);
        if(isBot) return details.channel.send("You don't have a chance against these bots anyway. Why bother?").then(msg => msg.delete({timeout: 5000}));

        let gameAlreadyExists = false;
        games.forEach(game => {
            if((game.player1.id === details.author.id || game.player1.id === otherPlayerID) && 
               (game.player2.id === details.author.id || game.player2.id === otherPlayerID)) {

                details.channel.send("Sorry, but this game already exists, please finish it first!")
                    .then(msg => msg.delete({timeout: 5000}));
                gameAlreadyExists = true;
                return;

            }
        });
        if(gameAlreadyExists) return;
        
        let turn = Math.random() < 0.5;
        const player1 = {
            id: details.author.id,
            fields: []
        }

        const player2 = {
            id: otherPlayerID,
            fields: []
        }

        const winningPosition = (fields) => {
            const entryExists = (n) => {
                return fields.indexOf(n) > -1;
            }

            return (entryExists(0) && entryExists(1) && entryExists(2)) ||
                (entryExists(3) && entryExists(4) && entryExists(5)) ||
                (entryExists(6) && entryExists(7) && entryExists(8)) ||
                (entryExists(0) && entryExists(3) && entryExists(6)) ||
                (entryExists(1) && entryExists(4) && entryExists(7)) ||
                (entryExists(2) && entryExists(5) && entryExists(8)) ||
                (entryExists(0) && entryExists(4) && entryExists(8)) ||
                (entryExists(2) && entryExists(4) && entryExists(6));
        }

        const blank = "    ";
        const vBar = "|";
        const hBar = "-";
        const circle = " 𐩒 ";
        const cross = " 🗙 ";
        let baseField = 
        "```\n" +
        blank + vBar + blank + vBar + blank + "\n" +
        hBar.repeat(14) + "\n" +
        blank + vBar + blank + vBar + blank + "\n" +
        hBar.repeat(14) + "\n" +
        blank + vBar + blank + vBar + blank + "\n" +
        "```"

        games.push({
            player1: player1, 
            player2: player2,
            turn: turn,
            field: baseField
        })

        let player1Name;  
        await tools.getUser.byID(details, player1.id).then(user => player1Name = user.username);
        
        let player2Name; 
        await tools.getUser.byID(details, player2.id).then(user => player2Name = user.username);

        const embed = new discord.MessageEmbed()
            .setColor("#aef")
            .setTitle(`Tic-Tac-Toe: ${player1Name} VS ${player2Name}`)
            .setDescription(`Turn: ${turn ? player2Name : player1Name}`)
            .addField("Just press a reaction to enter your turn!", baseField)
            .addField("\u200B", "\u200B")
            .setTimestamp()
            .setFooter("Never going to give you up!");   
        const originalEmojis = ["1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣"]
        let emojis = originalEmojis;
        const botMsg = await details.channel.send(embed);
        await originalEmojis.forEach(emoji => botMsg.react(emoji));
        await details.channel.send("Have fun!");

        var reactionListener = async (reaction, user) => {
            if(botMsg !== reaction.message) return;
            if(user === bot.user) return;
            if(user.id !== player1.id && user.id !== player2.id) {
                return details.channel.send(`Stop messing around ${tools.mention.user(user.id)}.`).then(msg => msg.delete({timeout: 5000}));
            }
            if(!emojis.includes(reaction.emoji.name)) return reaction.remove(user.id);
            
            if(turn && user.id === player1.id) 
                return details.channel.send(`This isn't your turn ${tools.mention.user(player1.id)}`)
                    .then(message => message.delete({ timeout: 2000}));
            if(!turn && user.id === player2.id) 
                return details.channel.send(`This isn't your turn ${tools.mention.user(player2.id)}`)
                    .then(message => message.delete({ timeout: 2000}));
            

            const eField = emojis.indexOf(reaction.emoji.name);
            const eIndex = originalEmojis.indexOf(reaction.emoji.name);
            let nth = 0;
            if(turn) {
                baseField = baseField.replace(/   /g, (match, i, original) => {
                    if(nth === eField) {
                        nth++;
                        return circle;
                    }
                    nth++;
                    return match;

                });
                player2.fields.push(eIndex);
            } else {
                baseField = baseField.replace(/   /g, (match, i, original) => {
                    if(nth === eField) {
                        nth++;
                        return cross;
                    }
                    nth++;
                    return match;

                });
                player1.fields.push(eIndex);
            }

            turn = !turn;
            embed.description = `Turn: ${turn ? player2Name : player1Name}`;
            embed.fields[0].value = baseField;
            botMsg.edit(embed);
            reaction.remove();
            emojis = emojis.filter((emoji) => emoji != reaction.emoji.name);

            if(winningPosition(player1.fields)) {
                embed.fields.push({
                    name: "A WINNER HAS BEEN CHOSEN!",
                    value: `Congratulations ${tools.mention.user(player1.id)}! You are truly superior to ${tools.mention.user(player2.id)}`
                });
                botMsg.edit(embed);
                bot.removeListener("messageReactionAdd", reactionListener);
                games = games.filter(game => game.player1.id != player1.id || game.player2.id != player2.id);
                return;
            }

            else if(winningPosition(player2.fields)) {
                embed.fields.push({
                    name: "A WINNER HAS BEEN CHOSEN!",
                    value: `Congratulations ${tools.mention.user(player2.id)}! You are truly superior to ${tools.mention.user(player1.id)}`
                });
                botMsg.edit(embed);
                bot.removeListener("messageReactionAdd", reactionListener);
                games = games.filter(game => game.player1.id != player1.id || game.player2.id != player2.id);
                return;
            }

            if(emojis.length == 0) {
                embed.fields.push({
                    name: "THE COMPETETANTS TIED THE MATCH!",
                    value: `Who knows how the next match with ${tools.mention.user(player1.id)} vs. ${tools.mention.user(player2.id)} will look like?`
                });
                botMsg.edit(embed);
                bot.removeListener("messageReactionAdd", reactionListener);
                games = games.filter(game => game.player1.id != player1.id || game.player2.id != player2.id);
                return;
            }

        };

        bot.on("messageReactionAdd", reactionListener);

    }
}