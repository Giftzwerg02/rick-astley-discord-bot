const Shape = require("./Shape");

module.exports = class Circle extends Shape {

    constructor(x, y, r, style = Shape.noStyle) {
        super(style);
        this.x = x;
        this.y = y;
        this.r = r;

        this.design = (context) => {
            
            this.loadStyle(context, style);
            context.ellipse(this.x, this.y, this.r, this.r, 0, 0, Math.PI * 2);
            this.pasteStyle(context);

        }

    }

    random(size) {

        this.x = Math.floor(Math.random() * size);
        this.y = Math.floor(Math.random() * size);
        this.r = Math.floor(Math.random() * size/4);

        this.style = {
            fill: this.getRandomColor(),
            stroke: this.getRandomColor(),
            width: Math.floor(Math.random() * 5)
        }

        return this;

    }

}