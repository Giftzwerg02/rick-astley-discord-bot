module.exports = class Designer {
    
    #Canvas = require("canvas");
    #shapes = [];

    constructor(discord) {
        this.discord = discord;
    }

    addShape(shape) {
        this.#shapes.push(shape);
    }

    addShapes(...shapes) {
        shapes.forEach(shape => this.addShape(shape));
    }

    drawAttachment(w, h, name, backgroundColor = null) {

        const canvas = this.#Canvas.createCanvas(w, h);
        const context = canvas.getContext("2d");

        if(backgroundColor) {
            context.beginPath();
            context.fillStyle = backgroundColor;
            context.fillRect(0, 0, w, h);
            context.closePath();
        }

        this.#shapes.forEach(shape => {
            context.beginPath();
            shape.renderShape(context);
            context.closePath();
        });

        return new this.discord.MessageAttachment(canvas.createPNGStream(), name);

    }

}