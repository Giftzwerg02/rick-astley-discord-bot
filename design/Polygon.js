const Shape = require("./Shape");

module.exports = class Polygon extends Shape {

    constructor(points = [], style = Shape.noStyle) {
        super(style);
        this.points = points;

        this.design = (context) => {
            
            this.loadStyle(context, style);
            
            const start = this.points.shift();
            
            context.moveTo(start.x, start.y);

            this.points.forEach(point => {
                context.lineTo(point.x, point.y);
            });
            this.pasteStyle(context);

        }

    }

    random(size) {

        const randomPointCount = Math.floor(Math.random() * 10) + 3;
        for(let i = 0; i < randomPointCount; i++) {
            this.points.push(this.randomPoint(size));
        }

        this.style = {
            fill: this.getRandomColor(),
            stroke: this.getRandomColor(),
            width: Math.floor(Math.random() * 5)
        }

        return this;

    }

    randomPoint(size) {
        return {
            x: Math.floor(Math.random() * size),
            y: Math.floor(Math.random() * size)
        }
    }

}