const Shape = require("./Shape");

module.exports = class Rectangle extends Shape {

    constructor(x, y, w, h, style = Shape.noStyle) {
        super(style);
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        this.design = (context) => {

            this.loadStyle(context);
            context.rect(this.x, this.y, this.w, this.h);
            this.pasteStyle(context);

        };

    }

    random(size) {

        this.x = Math.floor(Math.random() * size);
        this.y = Math.floor(Math.random() * size);
        this.w = Math.floor(Math.random() * size/4);
        this.h = Math.floor(Math.random() * size/4);

        this.style = {
            fill: this.getRandomColor(),
            stroke: this.getRandomColor(),
            width: Math.floor(Math.random() * 5)
        }

        return this;

    }
    

}