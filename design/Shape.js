module.exports = class Shape {

    static noStyle = {fill: null, stroke: null, width: null};

    constructor(style) {
        this.style = style;
    }

    renderShape(context) {
        this.design(context);
    }

    loadStyle(context) {
        if(this.style.fill) context.fillStyle = this.style.fill;
        if(this.style.stroke) context.strokeStyle = this.style.stroke;
        if(this.style.width) context.lineWidth = this.style.width;
    }

    pasteStyle(context) {
        context.stroke();
        context.fill();
    }

    getRandomColor() {
        return "#"+(Math.random()*0xFFFFFF<<0).toString(16);
    }

}