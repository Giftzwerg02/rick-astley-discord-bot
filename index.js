const discord = require("discord.js");
const dotenv = require("dotenv");
const WebSocket = require("./web/web");
const mongoose = require("mongoose");
const Designer = require("./design/Designer");

const tools = {};
const fs = require("fs");
fs.readdirSync("tools").forEach(file => {
    const tool = require("./tools/" + file);
    tools[tool.name] = tool.func;
});

const models = {};
fs.readdirSync("models").forEach(file => {
    const model = require("./models/" + file);
    models[model.name] = model.mod;
});

const shapes = {
    getDesigner: () => new Designer(discord)
}
fs.readdirSync("design").forEach(file => {
    if(file === "Designer.js" || file === "Shape.js") return;
    const Shape = require("./design/" + file);
    const shapeName = file.replace(".js", "");
    shapes[shapeName] = Shape;
})

const prefix = ".";
module.exports = prefix;
const commands = require("./commands.js");

const client = new discord.Client({
    disableEveryone: true
});

dotenv.config({
    path: __dirname + "/.env"
});

client.on("ready", () => {
    console.log("I am online!");    
    client.user.setStatus("idle");
    client.user.setPresence({
        activity: {
            name: "Never gonna give you up!",
            type: "PLAYING"
        }
    });

});


onMessageFiltered((info, command) => {
    if(isExistingCommand(command)) {
        commands[command](info);
    }
});

console.log("Bot fully loaded!");
client.login(process.env.TOKEN);


function onMessageFiltered(messageFunc) {
    function getCmdWithArgs(commandWithPrefix) {

        let splitted = commandWithPrefix.slice(prefix.length).trim().split(" ");
        let command = splitted.shift();
        let args = splitted;

        return {
            command: command,
            args: args
        };
    }
    
    client.on("message", async details => {

        if(details.author.bot) return;
        if(!details.content.startsWith(prefix)) return;
        
        const args = getCmdWithArgs(details.content);
        const info = {
            discord: discord,
            mongoose: mongoose,
            connectTo: (table) => mongoose.connect("mongodb://localhost:27017/" + table, { useNewUrlParser: true, useUnifiedTopology: true }),       
            details: details,
            bot: client,
            args: args.args,
            tools: tools,
            models: models,
            shapes: shapes
        };    
        messageFunc(info, args.command);
    
    });
}

function isExistingCommand(command) {
    return Object.getOwnPropertyNames(commands).includes(command);
}

const websocket = new WebSocket("123456", 8989, client, commands, discord, tools, models, shapes);