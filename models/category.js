const mongoose = require("mongoose");

const schema =  mongoose.Schema({
    index: Number,
    name: String,
    _fk_id: [{type: mongoose.Schema.Types.ObjectId, ref: "Subject"}]
});

module.exports = {
    name: "Category",
    mod: mongoose.model("Category", schema)
}