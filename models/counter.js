const mongoose = require("mongoose");

const schema =  mongoose.Schema({
    userID: String,
    username: String,
    count: Number
});

module.exports = {
    name: "Counter",
    mod: mongoose.model("Counter", schema)
}
