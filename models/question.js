const mongoose = require("mongoose");

const schema =  mongoose.Schema({
    question: String,
    answer: String,
    _fk_id: [{type: mongoose.Schema.Types.ObjectId, ref: "Category"}]
});

module.exports = {
    name: "Question",
    mod: mongoose.model("Question", schema)
}