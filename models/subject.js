const mongoose = require("mongoose");

const schema =  mongoose.Schema({
    index: Number,
    name: String,
    fullN: String
});

module.exports = {
    name: "Subject",
    mod: mongoose.model("Subject", schema)
}