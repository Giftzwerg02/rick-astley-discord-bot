module.exports = {
    name: "getUser",
    func: {
        byID: (details, id) => {
            return details.client.users.fetch(id);
        }
    }
}