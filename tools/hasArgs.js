module.exports = {
    name: "hasArgs",
    func: (args) => {
        if(!args) return false;
        return args.join(" ").trim() !== "";
    }
}