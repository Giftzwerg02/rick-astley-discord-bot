const mentionUserRegExp = /\<\@\!(\d*?)\>/;

module.exports = {
    name: "mention",
    func: {
        user: (id) => {
            return `<@${id}>`;
        },

        isUserMention: (str) => {
            return mentionUserRegExp.test(str);
        },

        getUserID: (mention) => {
            return mentionUserRegExp.exec(mention)[1];
        } 
    }
}