module.exports = {
    name: "owneronly",
    func: (details) => {
        if(details.author.id !== "358246557772021773") {
            details.channel.send("I am sorry, but this command is only available for the owner of this bot.");
            return false;
        }
        return true;
    }
}