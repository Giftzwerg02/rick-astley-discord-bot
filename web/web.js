const express = require("express");
const hbs = require("express-handlebars");
const bodyParser = require("body-parser");
const path = require("path");
const dotenv = require("dotenv");
dotenv.config({
    path: __dirname + "/.env"
});

class WebSocket {

    username = "";
    password = "";
    admin = false;

    constructor(token, port, client, commands, discord, tools, models, shapes) {
        this.token = token;
        this.client = client;
        this.commands = commands;
        this.discord = discord;
        this.tools = tools;
        this.models = models;
        this.shapes = shapes;
        
        this.app = express();
        this.app.engine("hbs", hbs({
            extname: "hbs",
            defaultLayout: "layout",
            layoutsDir: __dirname + "/layouts"
        }));
        this.app.set("views", path.join(__dirname, "views"));
        this.app.set("view engine", "hbs");
        this.app.use(express.static(path.join(__dirname, "public")));
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());

        this.loadIndex();
        this.onLogin();
        this.loadDashboard();
        this.onSendMessage();
        this.onExecuteCommand();

        this.server = this.app.listen(process.env.PORT || port, () => {
            console.log(`Websocket listening on port ${this.server.address().port}`);
        });
                
    }

    loadIndex() {
        this.app.get("/", (req, res) => {

            if(this.isAdminUser()) {
                this.admin = true;
                res.render("index", { title: "ADMIN", channels: this.getChannels() });
                return;
            } else {
                res.render("login", { title: "Login" });
                return;
            }

        });

    }
    
    onLogin() {
        this.app.post("/", (req, res) => {
            this.username = req.body.username;
            this.password = req.body.password;
            if(this.isAdminUser()) {
                this.admin = true;
                res.render("index", { title: "ADMIN", channels: this.getChannels()});
                return;
            }
            res.render("index", { title: "Rick Astley on-line!", channels: this.getChannels() });            
        });
    }

    getChannels() {
        let channels = [];
        this.client.guilds.cache.get("628634449072095290").channels.cache
            .filter(channel => channel.type === "text")
            .forEach(channel => {
                channels.push({
                    id: channel.id,
                    name: channel.name
                });
            });
        return channels;
    }

    isAdminUser() {
        return this.username === process.env.USER && this.password === process.env.PASS;
    }

    loadDashboard() {
        this.app.get("/dashboard", (req, res) => {
            
            res.render("dashboard", { 
                title: "RickAstley on-line!",
                commands: this.commands,
                channels: this.getChannels()
            });

        });
    }

    onExecuteCommand() {
        this.app.post("/executeCommand", async (req, res) => {
            const command = req.body.command;
            const channelid = req.body.channelid;
            const args = req.body.text.split(" ");
            const selfID = req.body.selfID;
            const channel = await this.client.channels.fetch(channelid);

            const user = this.client.users.cache.get(selfID);

            let details = {
                channel: channel,
                author: {
                    id: selfID
                },
                client: this.client
            };

            const info = {
                discord: this.discord,
                details: details,
                args: args,
                bot: this.client,
                tools: this.tools,
                models: this.models,
                shapes: this.shapes
            };

            this.commands[command](info);
        
            res.sendStatus(200);
        });
    }
    
    onSendMessage() {

        this.app.post("/sendMessage", (req, res) => {
            
            const text = req.body.text;
            const channelid = req.body.channelid;
            
            const channel = this.client.guilds.cache.get("628634449072095290").channels.cache.get(channelid);

            channel.send(text);
            
            res.sendStatus(200);

        });
            
    }


}

module.exports = WebSocket;